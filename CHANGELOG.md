## [1.11.6](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.11.5...v1.11.6) (2020-07-21)


### Performance Improvements

* **inventory:** upsert edges to prevent thousands up Updated edges ([d3ae83b](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/d3ae83b))



## [1.11.5](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.11.4...v1.11.5) (2020-05-13)


### Performance Improvements

* **tasks:** improve allocated inventory query ([32c0d77](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/32c0d77))



## [1.11.4](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.11.3...v1.11.4) (2020-04-02)


### Bug Fixes

* **ToThreePLOrderContactInfo, ToBigCommerceShipment:** fix issues caused by imperfect data ([b8c5edb](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/b8c5edb))



## [1.11.3](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.11.2...v1.11.3) (2020-03-09)


### Bug Fixes

* **conversions:** change query to unicode string ([f25041c](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/f25041c))



## [1.11.2](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.11.1...v1.11.2) (2019-12-18)


### Bug Fixes

* **ToThreePLOrderContactInfo:** remove broken logging ([dbc471e](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/dbc471e))



## [1.11.1](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.11.0...v1.11.1) (2019-12-18)


### Bug Fixes

* **conversions:** filter items with a Qty of 0 ([c96b478](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/c96b478))
* **ToBigCommerceShipment:** fix issue with potentially missing state code ([4e07eb4](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/4e07eb4))


### Features

* **app:** allow disabling inventory syncs ([af9864d](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/af9864d))



## [1.10.1](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.10.0...v1.10.1) (2019-11-05)


### Bug Fixes

* **app.py:** Fix _get_order_subquery ([8cbf532](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/8cbf532))



# [1.10.0](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.9.0...v1.10.0) (2019-10-28)


### Bug Fixes

* **conversion:** fix logger reference ([a73464a](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/a73464a))
* **conversions.py:** Fix a sync error for orders with only digital products ([43271d4](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/43271d4))
* prevent conflicts with other integrations ([bdb4e40](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/bdb4e40))



# [1.9.0](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.8.0...v1.9.0) (2019-06-26)


### Bug Fixes

* **scylla_bigcommerce_threepl/conversions.py:** change will remove white space from state and country ([d3dffda](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/d3dffda))


### Features

* **ToThreePLOrder:** Pass staff notes from BC to 3PL ([e5d3983](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/e5d3983))



# [1.8.0](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.7.1...v1.8.0) (2019-06-14)


### Bug Fixes

* **scylla_bigcommerce_threepl/conversions.py:** Fixed error with BC sync to ThreePL ([764b0dd](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/764b0dd))



## [1.7.1](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.7.0...v1.7.1) (2019-04-23)


### Bug Fixes

* **ToBigCommerceShipment:** Filter out line items that weren't created from BigCommerce ([134ef9e](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/134ef9e))



# [1.7.0](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.6.2...v1.7.0) (2019-04-15)


### Features

* **ToThreePLOrder:** Add the ability to add additional sku's if they're in stock ([9514732](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/9514732))



## [1.6.2](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.6.1...v1.6.2) (2019-04-15)


### Bug Fixes

* **ToThreePLOrder:** Fix variable name in post_convert ([db52136](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/db52136))



## [1.6.1](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.6.0...v1.6.1) (2019-04-15)


### Bug Fixes

* **ToThreePLOrder:** Fix iteration on integer bug ([251a109](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/251a109))



# [1.6.0](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.5.2...v1.6.0) (2019-04-15)


### Features

* **ToThreePLOrder:** Add earliestShipDate to conversion ([dceb944](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/dceb944))
* **ToThreePLOrder:** Add error checking to order conversion ([af415e4](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/af415e4))



## [1.5.2](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.5.1...v1.5.2) (2019-03-28)


### Bug Fixes

* **ToThreePLRoutingInfo, ToThreePLOrder, App:** Fix issue with unmappable shipping methods ([d870145](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/d870145))



## [1.5.1](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.5.0...v1.5.1) (2019-03-27)


### Bug Fixes

* **App:** Filter out cancelled orders from fulfillment ([361ed6e](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/361ed6e))



# [1.5.0](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.4.0...v1.5.0) (2019-02-18)


### Features

* **ToThreePLOrderContactInfo:** Exclude certain states from the state code mapping as they don't ha ([d573198](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/d573198))



# [1.4.0](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.3.3...v1.4.0) (2019-02-14)


### Features

* **ToThreePLRoutingInfo:** Pass through raw shipping method for mapping in 3PL if no mapping exists ([50a8127](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/50a8127))



## [1.3.3](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.3.2...v1.3.3) (2019-02-13)


### Bug Fixes

* **App:** Fix issue where shipment creation is being run for 3PL orders with no BC order ([de70b8c](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/de70b8c))



## [1.3.2](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.3.1...v1.3.2) (2019-02-12)


### Bug Fixes

* **App:** Fix where clause and ([2a7fe72](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/2a7fe72))



## [1.3.1](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.3.0...v1.3.1) (2019-02-12)


### Bug Fixes

* **App:** Add missing "AND" to where statement ([dbc3d3f](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/dbc3d3f))



# [1.3.0](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.2.0...v1.3.0) (2019-02-01)


### Features

* **ToThreePLRoutingInfo, App:** Do not default shipping method and force manual update when no matc ([9394d22](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/9394d22))



# [1.2.0](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.1.0...v1.2.0) (2019-01-31)


### Features

* **ToThreePLOrder, ToThreePLRoutingInfo:** Allow a shipping map to be injected from configuration ([3a392ff](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/3a392ff))



# [1.1.0](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.0.2...v1.1.0) (2019-01-21)


### Features

* **ToBigCommerceShipment:** Update the ToBigCommerceShipment conversion to account for aliased sku' ([2545d54](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/2545d54))



## [1.0.2](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.0.1...v1.0.2) (2019-01-16)


### Bug Fixes

* **requirements.txt:** Bump patch version of scylla-threepl ([ac41fd3](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/ac41fd3))



## [1.0.1](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/v1.0.0...v1.0.1) (2019-01-14)


### Bug Fixes

* **ToBigCommerceOrder:** Fixed issue of pushing digital products to 3PL and not adding customer ship ([f71cd49](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/f71cd49))



# [1.0.0](https://gitlab.com/5stones/scylla-bigcommerce-threepl/compare/915444c...v1.0.0) (2019-01-08)


### Features

* ***/*:** Create basic integration between 3pl and BigCommerce ([915444c](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/915444c))
* **LinkInventoryTask, UpdateBigCommerceInventoryTask, ToBigCommerceInventory:** Add tasks to sync i ([ae2c67d](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/ae2c67d))
* **LinkInventoryTask, UpdateBigCommerceSkuInventoryTask:** Add tasks for the syncing of Sku invento ([a79f76a](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/a79f76a))
* **ToBigCommerceShipmentTask:** Update a newly created shipment with the id of it's order ([6052c8d](https://gitlab.com/5stones/scylla-bigcommerce-threepl/commit/6052c8d))



