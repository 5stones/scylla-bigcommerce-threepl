# -*- coding: utf-8 -*-
import json
import os
import unittest
from scylla_bigcommerce_threepl import conversions


class TestConversions(unittest.TestCase):
    """Tests for BigCommerce-ThreePL conversions."""
    path = os.path.dirname(os.path.abspath(__file__))
    maxDiff = None

    def test_line_items(self):
        with open(self.path + '/order-items.json') as json_file:
            order_items = json.load(json_file)

        conv = conversions.ToBigCommerceShipmentLineItem()
        bc_shipment_items = conv.convert_list(order_items)
        bc_shipment_items.sort()

        expected_lines = [
            {'order_product_id': "6619", 'quantity': 1}, ]
        expected_lines.sort()
        self.assertEqual(bc_shipment_items, expected_lines)


if __name__ == '__main__':
    unittest.main()
