from distutils.core import setup
import json

with open('package.json', 'r') as f:
    package_json = json.load(f)

version = package_json['version']

setup(
    name = 'scylla-bigcommerce-threepl',
    packages = ['scylla_bigcommerce_threepl'],
    version = version,

    description = 'Scylla-BigCommerce-ThreePL provides the basic utilities to integrate BigCommerce with the standard 3PL Central API',

    #author = '',
    #author_email = '',

    url = 'https://git@gitlab.com:5stones/scylla-bigcommerce-threepl',
    download_url = 'https://gitlab.com/5stones/scylla-bigcommerce-threepl/repository/archive.tar.gz?ref=' + version,

    keywords = 'integration scylla threepl bigcommerce',

    classifiers=[
        #'Development Status :: 4 - Beta',
        #'Development Status :: 5 - Production/Stable',

        #'Intended Audience :: Developers',
        #'Topic :: Software Development :: Build Tools',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
    ],

    install_requires = [
        'requests',
        'scylla',
        'scylla_bigcommerce',
        'scylla_threepl',
    ],
    dependency_links=[
        'git+https://gitlab.com/5stones/scylla.git',
        'git+https://gitlab.com/5stones/scylla-bigcommerce.git',
        'git+https://gitlab.com/5stones/scylla-threepl.git',
    ],
)
