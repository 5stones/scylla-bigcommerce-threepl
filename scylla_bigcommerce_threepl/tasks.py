# coding: utf8
import logging
from scylla import tasks
from scylla import orientdb
from scylla import graceful
import scylla_bigcommerce as bigcommerce
from .conversions import ToBigCommerceShipment


class ToBigCommerceShipmentTask(bigcommerce.ToBigCommerceShipmentTask):
    key_field = 'OrderId'
    fetchplan = '*:-1 [*]in_*:-2 [*]out_*:-2 $bc_order.shipping_addresses:1 $bc_order.products:1'

    def __init__(self,
        from_class,
        client,
        conversion=ToBigCommerceShipment(),
        where=None,
        with_reflection=False):

        super(ToBigCommerceShipmentTask, self).__init__(
            from_class,
            client,
            conversion=conversion,
            where=where,
            with_reflection=with_reflection)

    def _after_save(self, from_obj, result_rec, is_update=False, request=None):
        # link the newly created shipment back to the order that generated it
        rid = from_obj.get('$bc_order')[0].get('@rid')
        q = "UPDATE {} SET _parent = {}".format(result_rec.rid, rid)
        orientdb.execute(q)

    def _get_order_id(self, obj, data=None):
        return obj.get('ExternalId')

    def _get_shipment_id(self, obj, data=None):
        return obj.get('ReferenceNum')



class LinkInventoryTask(tasks.ReflectTask):
    """Link 3PL inventory summaries to BigCommerce Products
    """
    key_field = 'ItemId'

    _logger = logging.getLogger('scylla-bigcommerce-threepl')

    def __init__(self, from_class, to_class, where=None, with_reflection=None):
        super(LinkInventoryTask, self).__init__(from_class, to_class, where=where, with_reflection=with_reflection)

        # manually specify task_id to avoid task name collision
        task_id = '{}{} > {}: link'.format(from_class[0], from_class[1], self.to_class)
        self.task_id = task_id

    def _get_filter_ids(self):
        """Build an array of ids that match sku's in BigCommerce"""
        if not hasattr(self, '_filter_ids_cache'):
            q = (
                "SELECT Sku, ItemId, $bc "
                "FROM {from_prefix}Item "
                "LET $bc = ( "
                    "SELECT FROM {to_class} "
                    "WHERE sku = $parent.current.Sku "
                    "AND (@this.BOTH('Reflection')[@class='{from_class}'].size() = 0)  "
                ") "
                "WHERE $bc.size() > 0 "
            ).format(from_prefix=self.from_module, to_class=self.to_class, from_class=self.from_class)
            results = orientdb.execute(q)

            self._filter_ids_cache = [r['ItemId'] for r in results]

        return self._filter_ids_cache

    def _format_where(self, after=None, rec_id=None):
        where = super(LinkInventoryTask, self)._format_where(after=after, rec_id=rec_id)
        filter_ids = self._get_filter_ids()
        # convert to strings for later .join() call
        filter_ids = [str(id) for id in filter_ids]

        if where == '' and len(filter_ids) > 0:
            where = 'WHERE ItemId IN [{}]'.format(','.join(filter_ids))
        elif len(filter_ids) > 0:
            where = '{} AND ItemId IN [{}]'.format(where, ','.join(filter_ids))

        return where

    def _process_response_record(self, obj):
        q = (
            "SELECT id "
            "FROM {to_class} "
            "WHERE sku = first(( "
                "SELECT Sku  "
                "FROM {from_module}Item "
                "WHERE ItemId={item_id} "
                "LIMIT 1 "
            ")).Sku "
            "LIMIT 1 "
        ).format(to_class=self.to_class, from_module=self.from_module, item_id=obj.get('ItemId'))

        result = orientdb.execute(q)
        if not result:
            self._logger.info(u'Skipping %s %s as no %s found', self.from_class, obj.get('@rid'), self.to_class)
            return None

        q = (
            "SELECT @rid "
            "FROM {to_class} "
            "WHERE id = {id} "
            "LIMIT 1"
        ).format(to_class=self.to_class, id=result[0].get('id'))

        result = orientdb.execute(q)
        if not result:
            self._logger.info(u'Skipping %s %s as no %s found', self.from_class, obj.get('@rid'), self.to_class)
            return None

        to_rid = result[0].get('rid')
        self._save_response(obj, to_rid)

    def _save_response(self, from_obj, to_rid):
        reflection_type = 'Updated'

        # save the response to orientdb
        with graceful.Uninterruptable():

            # draw the edge
            orientdb.create_edge(from_obj['@rid'], to_rid,
                reflection_type, content=None)

            self._logger.info(
                u'%s %s %s  –%s→  %s %s',
                from_obj['@rid'], from_obj['@class'], from_obj.get('id', ''),
                reflection_type,
                self.to_class, to_rid,
            )


class UpdateBigCommerceInventoryTask(bigcommerce.ToBigCommerceProductTask):
    """Updates inventory levels in BigCommerce based on 3PL values
    """
    key_field = 'ItemId'
    upsert_edge = True

    def _step(self, after, options):
        self._cache = self._get_inventory_adjustment_cache()
        super(UpdateBigCommerceInventoryTask, self)._step(after, options)

    def _process_response_record(self, obj):
        """Hits a bigcommerce endpoint for the result
        """
        # create the record in bigcommerce
        if self.conversion:
            id = obj.get('_linked_to').get('id')

            # lazy load the cache if it isn't already created
            if not hasattr(self, '_cache'):
                self._cache = self._get_inventory_adjustment_cache()

            adjustment = self._cache.get(str(id), 0)
            data = self.conversion(obj, adjustment)
        else:
            data = None

        is_update = obj.get('_linked_to', '') != ''
        bc_result = self._to_bigcommerce(obj, is_update=is_update, data=data)

        # save the bigcommerce response to orient and link it
        bc_rec = bigcommerce.records.BigCommerceRecord.factory(self.client, self._get_factory_type(), bc_result)
        self._save_response(obj, bc_rec, is_update=is_update, request=data)

    def _get_inventory_adjustment_cache(self):
        """Create a cache of the inventory that's been allocated in bigcommerce but not pushed into 3pl
        """
        # select any completed orders that haven't been pushed into 3pl
        unsyncd_q = (
            "SELECT "
            "FROM {to_prefix}Order "
            "WHERE status_id = 11 "
            "AND out().out('Created')[@class instanceof '{from_prefix}Order'].size() = 0 "
            "AND out('Created')[@class instanceof '{from_prefix}Order'].size() = 0 "
        )

        line_items_q = (
            "SELECT products "
            "FROM ( "
                + unsyncd_q +
            ") "
            "UNWIND products "
        )

        q = (
            "SELECT products.id AS id, sum(products.quantity) AS qty "
            "FROM ( "
                + line_items_q +
            ") "
            "GROUP BY products.id "
        ).format(to_prefix=self.to_module, from_prefix=self.from_module)

        results = orientdb.execute(q)
        cache = {}

        for result in results:
            id = str(result.get('id'))
            qty = int(result.get('qty'))
            cache[id] = qty

        return cache


class UpdateBigCommerceSkuInventoryTask(UpdateBigCommerceInventoryTask):

    def __init__(self,
        from_class,
        client,
        to_type='Sku',
        url_format='/products/{}/skus/{}',
        conversion=None,
        where=None,
        with_reflection=False):

        super(UpdateBigCommerceSkuInventoryTask, self).__init__(
            from_class,
            client,
            to_type=to_type,
            url_format=url_format,
            conversion=conversion,
            where=where,
            with_reflection=with_reflection
        )

    def _get_url(self, obj, is_update=False, data=None):
        id = ''
        sku = obj.get('_linked_to')
        product_id = sku.get('product_id')
        if is_update:
            id = sku.get('id')

        return self.url_format.format(product_id, id)
