import collections
from scylla import app
from scylla import configuration
import scylla_bigcommerce as bigcommerce
import scylla_threepl as threepl
from . import conversions
from . import tasks


configuration.setDefaults('BigCommerce-ThreePL', {
    'reference_prefix': 'BC',
    'sync_inventory': True,
})


class App(app.App):

    def _prepare(self):
        self.bigcommerce_client = self._get_bigcommerce_client()
        self.threepl_client = self._get_threepl_client()

        tpl_config = configuration.getSection('ThreePL')
        bc_config = configuration.getSection('BigCommerce')

        self.tasks['bigcommerce-order'] = threepl.ToThreePLOrderTask(
            (bc_config.get('name'), 'Order'),
            conversions.ToThreePLOrder(
                tpl_config.get('customer_id'),
                additional_skus=tpl_config.get('per_order_additional_skus', [])
            ),
            self.threepl_client,
            'Order',
            where=self._get_order_where(bc_config),
            # only shipments not sync'd to threepl
            with_reflection=False
        )

        self.tasks['create-shipment'] = tasks.ToBigCommerceShipmentTask(
            (tpl_config.get('name'), 'Order'),
            self.bigcommerce_client,
            where=(
                "_ProcessDate IS NOT NULL "
                "AND ReadOnly.IsClosed = true "
                "AND ReadOnly.Status = 1 "
                "AND ReferenceNum LIKE '{}-%' "
                "AND $bc_order.size() > 0 "
            ).format(configuration.get('BigCommerce-ThreePL', 'reference_prefix')),
        ).add_let('$bc_order', self._get_order_subquery())

        if configuration.get('BigCommerce-ThreePL', 'sync_inventory'):
            self.tasks.update(self._get_inventory_syncs())
        else:
            self.on_demand_tasks.update(self._get_inventory_syncs())

    def _get_inventory_syncs(self):
        tpl_name = configuration.get('ThreePL', 'name')
        bc_name = configuration.get('BigCommerce', 'name')
        inv_tasks = collections.OrderedDict()

        inv_tasks['link-inventory'] = tasks.LinkInventoryTask(
            (tpl_name, 'StockSummary'),
            (bc_name, 'Product'),
            where=None,
            with_reflection=False,
        )

        inv_tasks['link-sku-inventory'] = tasks.LinkInventoryTask(
            (tpl_name, 'StockSummary'),
            (bc_name, 'Sku'),
            where=None,
            with_reflection=False,
        )

        inv_tasks['update-inventory'] = tasks.UpdateBigCommerceInventoryTask(
            (tpl_name, 'StockSummary'),
            self.bigcommerce_client,
            where=None,
            conversion=conversions.ToBigCommerceInventory(),
            with_reflection=True
        )

        inv_tasks['update-sku-inventory'] = tasks.UpdateBigCommerceSkuInventoryTask(
            (tpl_name, 'StockSummary'),
            self.bigcommerce_client,
            where=None,
            conversion=conversions.ToBigCommerceInventory(),
            with_reflection=True
        )

        return inv_tasks

    def _get_bigcommerce_client(self):
        config = configuration.getSection('BigCommerce')
        return bigcommerce.BigCommerceRestClient(
            config.get('name'),
            config.get('url'),
            config.get('client_secret'),
            config.get('client_id'),
            version=config.get('version'),
            verify_ssl=config.get('verify_ssl'),
            debug=config.get('debug')
        )

    def _get_threepl_client(self):
        config = configuration.getSection('ThreePL')
        return threepl.ThreePLRestClient(
            config['name'],
            config['url'],
            config['client_id'],
            config['client_secret'],
            config['three_pl_key']
        )

    def _get_order_where(self, config):
        where = (
            "status_id IN [11] "
        )

        exclusions = config.get('shipping_method_exclusions', [])

        if len(exclusions) > 0:
            formated_exclusions = ['\'{}\''.format(e) for e in exclusions]
            where = '{} AND shipping_addresses[0].shipping_method NOT IN [{}]'.format(where, ', '.join(formated_exclusions))

        return where

    def _get_order_subquery(self):
        return (
            "("
                "SELECT FROM {}Order "
                "WHERE id = $parent.current.ExternalId "
                "LIMIT 1 "
            ")"
        ).format(configuration.get('BigCommerce', 'name'))
