import logging
from scylla import convert
from scylla import configuration
from scylla import orientdb
from iso2_country_state_exclusions import iso2_country_state_exclusions


class ToThreePLLineItem(convert.Conversion):
    """Convert an Spree line item to an 3PL line item"""
    _logger = logging.getLogger('scylla-bigcommerce-threepl')

    def __init__(self):
        super(ToThreePLLineItem, self).__init__()

        self.conversion_map.update({
            'itemIdentifier': ('sku', self._get_item_identifier_by_variant_id, ),
            'qty': (self._get_qty, ),
            'externalId': ('id', ),
            'savedElements': (self._get_saved_elements, ),
        })

    def _get_item_identifier_by_variant_id(self, sku):
        item_identifier = { 'sku': sku }

        try:
            item_id = orientdb.lookup('ThreePLItem', 'Sku', sku, 'ItemId')
            item_identifier['id'] = item_id
        except orientdb.OrientDbError:
            self._logger.warning('Unable to locate ThreePLItem with sku %s. Attempting to send as an alias.', sku)

        return item_identifier

    @staticmethod
    def _get_qty(line):
        return line.get('quantity') - line.get('quantity_shipped', 0)

    @staticmethod
    def _get_saved_elements(item):
        return []


class ToThreePLOrderContactInfo(convert.Conversion):

    def __init__(self):
        super(ToThreePLOrderContactInfo, self).__init__()
        self.conversion_map.update({
            'companyName': '',
            'name': (self._get_full_name,  ),
            'title': '',
            'address1': ('street_1', ),
            'address2': (self._get_address2, ),
            'city': ('city', ),
            'state': (self._get_state_code, ),
            'zip': ('zip', ),
            'country': ('country_iso2', ),
            'phoneNumber': ('phone', ),
            'fax': '',
            'emailAddress': ('email', ),
            'dept': '',
            'code': '',
            'addressStatus': 0,
        })

    @staticmethod
    def _get_full_name(address):
        return u'{} {}'.format(address.get('first_name'), address.get('last_name'))

    @staticmethod
    def _get_state_code(address):
        bc_prefix = configuration.get('BigCommerce', 'name')
        country_iso2 = address.get('country_iso2').strip()
        state_name = address.get('state').strip()

        if country_iso2 in iso2_country_state_exclusions:
            # some countries don't have a proper state so pass the value straight through
            return state_name
        elif not state_name:
            # if for some reason we couldn't find a state code, just pass the state name value
            return state_name

        q = (
            u"SELECT "
            "FROM {prefix}State "
            "WHERE state = '{state}' OR state_abbreviation = '{state_uppercase}' "
            "AND country_id = first(( "
                "SELECT id "
                "FROM {prefix}Country "
                "WHERE country_iso2 = '{country}' "
                "LIMIT 1 "
            ")).id "
            "LIMIT 1 "
        ).format(prefix=bc_prefix, state=state_name, state_uppercase=state_name.upper(), country=country_iso2)

        results = orientdb.execute(q)
        if len(results) > 0:
            return results[0].get('state_abbreviation')
        else:
            # default if we can't find the state in the db
            raise orientdb.OrientDbError(u'Unable to determine abbreviation for the state: {}'.format(state_name))

    @staticmethod
    def _get_address2(address):
        if 'street_2' in address and address['street_2'] not in [None, '']:
            return address['street_2']
        else:
            return ''


class ToThreePLShipToInfo(ToThreePLOrderContactInfo):
    def __init__(self):
        super(ToThreePLShipToInfo, self).__init__()

        self.conversion_map.update({
            'companyName': (self._get_company_name, ),
        })

    @staticmethod
    def _get_company_name(address):
        if 'company' in address and address['company'] not in [None, '']:
            return address['company']
        else:
            return ToThreePLShipToInfo._get_full_name(address)


class ToThreePLRoutingInfo(convert.Conversion):

    def __init__(self):
        super(ToThreePLRoutingInfo, self).__init__()

        self.conversion_map.update({
            'isCod': False,
            'isInsurance': False,
            'requiresDeliveryConf': False,
            'requiresReturnReceipt': False,
            'mode': ('shipping_method', ),
        })

class ToThreePLOrder(convert.Conversion):
    """Convert an BigCommerce Order to a 3PL order"""

    from_type = 'Order'
    to_type = 'Order'

    def __init__(self, customer_id, additional_skus=[]):
        super(ToThreePLOrder, self).__init__()

        self.additional_skus = additional_skus
        line_conversion = ToThreePLLineItem()
        self.conversion_map.update({
            'customerIdentifier': { 'id': customer_id },
            'facilityIdentifier': { 'id': 1 },
            # 'warehouseTransactionSourceEnum': (),
            # 'transactionEntryTypeEnum': (),
            # 'importChannelIdentifier': (),
            # 'importModuleId': (),
            # 'exportModuleIds': (),
            # 'deferNotification': (),
            'referenceNum': ('id', self._get_formatted_reference_number, ),
            # 'description': (),
            # 'poNum': (),
            'externalId': ('id', ),
            'earliestShipDate': ('date_created', ),
            # 'shipCancelDate': (),
            'notes': ('staff_notes', ),
            # 'numUnits1': (),
            # 'unit1Identifier': (),
            # 'numUnits2': (),
            # 'unit2Identifier': (),
            # 'totalWeight': (),
            # 'totalVolume': (),
            # 'billingCode': (),
            # 'asnNumber': (),
            # 'upsServiceOptionCharge': (),
            # 'upsTransportationCharge': (),
            # 'addFreightToCod': (),
            # 'upsIsResidential': (),
            # 'exportChannelIdentifier': (),
            # 'routePickupDate': (),
            'shippingNotes': ('customer_message', ),
            # 'masterBillOfLadingId': (),
            'invoiceNumber': ('id', self._get_formatted_reference_number, ),
            # 'fulfillInvInfo': (),
            'routingInfo': ('shipping_addresses', 0, ToThreePLRoutingInfo(), ),
            'shipTo': ('shipping_addresses', 0, ToThreePLShipToInfo(), ),
            'soldTo': ('billing_address', ToThreePLOrderContactInfo(), ),
            'billTo': ('billing_address', ToThreePLOrderContactInfo(), ),
            'savedElements': (self._get_saved_elements, ),
            'orderItems': (
                'products',
                lambda items: filter(lambda item: item['type'] == 'physical', items),
                lambda items: [line_conversion(item) for item in items],
            ),
        })

    def convert(self, obj, included_fields=None):
        bc_total = len(obj.get('products', []))

        if bc_total == 0:
            raise Exception("The BigCommerce order being processed has no line items. Please re-download the order.")

        bc_lines = 0
        bc_skip = 0

        for item in obj.get('products', []):
            bc_lines += 1
            if item['type'] != 'physical':
                bc_skip += 1

        if bc_skip != bc_lines:
            threepl_order = super(ToThreePLOrder, self).convert(obj, included_fields)

            tpl_lines = len(threepl_order.get('orderItems', []))

            if (tpl_lines == 0):
                raise Exception('Attempting to send 0 line items to 3PL even though there are physical items on the order.')

            return threepl_order
        else:
            # If the order contains only digital products, do not send it to 3PL
            return False

    def _post_convert(self, bc_order, threepl_order):
        return self._add_additional_lines(bc_order, threepl_order, self.additional_skus)

    def _add_additional_lines(self, bc_order, threepl_order, additional_skus):

        for sku in additional_skus:
            item_id = self._get_item_id_if_available(sku)

            if item_id is not False:
                line_item = {
                    'itemIdentifier': { 'sku': sku, 'id': item_id },
                    'qty': 1,
                    'savedElements': [],
                }

                threepl_order.get('orderItems', []).append(line_item)

        return threepl_order

    def _get_item_id_if_available(self, sku):
        q = (
            "SELECT ItemId as item_id "
            "FROM ThreePLStockSummary "
            "WHERE _item.Sku = '{sku}' "
            "AND _available > 0 "
            "LIMIT 1 "
        ).format(sku=sku)

        results = orientdb.execute(q)
        if len(results) > 0:
            return results[0].get('item_id')
        else:
            return False

    @staticmethod
    def _get_formatted_reference_number(id):
        return '{}-{}'.format(configuration.get('BigCommerce-ThreePL', 'reference_prefix'), id)

    @staticmethod
    def _get_saved_elements(order):
        return []


class ToBigCommerceShipmentLineItem(convert.Conversion):
    """Convert a shipped 3PL order item to a BigCommerce shipment item"""

    def __init__(self):
        super(ToBigCommerceShipmentLineItem, self).__init__()

        self.conversion_map.update({
            'order_product_id': ('ExternalId', ),
            'quantity': ('Qty', ),
        })

    def convert_list(self, items):
        """Filter and convert a list of line items."""
        return [
            self(item) for item in items
            # If an item doesn't have an `ExternalID` on it then it should be skipped
            # as it wasn't created fromt he original order and therefore can't be fulfilled
            if item.get('ExternalId') and item.get('Qty', 0) > 0
        ]


class ToBigCommerceShipment(convert.Conversion):
    """Convert a shipped 3PL order to a BigCommerce shipment"""

    def __init__(self):
        super(ToBigCommerceShipment, self).__init__()

        line_conversion = ToBigCommerceShipmentLineItem()

        self.conversion_map.update({
            'tracking_number': ('RoutingInfo', 'TrackingNumber', ),
            'comments': '',
            'order_address_id': ('$bc_order', 0, 'shipping_addresses', 0, 'id'),
            'shipping_provider': (
                'RoutingInfo',
                'Carrier',
                lambda shipping_provider: shipping_provider if shipping_provider != 'InHouse' else '',
            ),
            'items': (
                # aggregated items created from an alias
                self._aggregate_aliased_items,
                # run the line item conversion on each line
                line_conversion.convert_list,
            ),
        })

    @staticmethod
    def _aggregate_aliased_items(order):
        items = order.get('OrderItems')
        items_without_external_id = []
        item_map = {}

        # create a hash of items based on the externalId
        # as duplicate externalId's means something was generated
        # from an alias in 3pl. Additionally, replace sku's with the aliased sku.
        for item in items:
            alias = ToBigCommerceShipment._get_aliased_sku_for_item(item)
            if alias is not None:
                item['ItemIdentifier']['Sku'] = alias
                # this id is irrelevant
                item['ItemIdentifier'].pop('Id')
                # override the qty with the original quantity ordered. Assume that we've shipped the full amount.
                item['Qty'] = ToBigCommerceShipment._get_original_qty_for_line(
                    item.get('ExternalId'), order.get('$bc_order', [])[0])

            external_id = item.get('ExternalId', None)
            if external_id is None:
                items_without_external_id.append(item)
            else:
                item_map[external_id] = item

        items = [item for external_id, item in item_map.items()]
        return items + items_without_external_id

    @staticmethod
    def _get_aliased_sku_for_item(item):
        for elem in item.get('SavedElements', []):
            if elem.get('Name', '') == 'CreatedFromAlias':
                return elem.get('Value')

        return None

    @staticmethod
    def _get_original_qty_for_line(line_id, bc_order):
        for line in bc_order.get('products', []):
            if str(line.get('id')) == str(line_id):
                return line.get('quantity') - line.get('quantity_shipped', 0)

        raise Exception('Unable to find original line item id {} for order {}'.format(
            line_id, bc_order.get('id')))

    @staticmethod
    def _get_qty(line):
        return line.get('quantity') - line.get('quantity_shipped', 0)

    @staticmethod
    def _get_saved_elements(item):
        return []


class ToBigCommerceInventory(convert.Conversion):
    """Convert an 3PL StockSummary to a BigCommerce inventory update"""

    from_type = 'StockSummary'
    to_type = 'Product'

    def __init__(self):
        super(ToBigCommerceInventory, self).__init__()

        self.conversion_map.update({
            'inventory_level': (self._get_available, ),
        })

    def __call__(self, obj, on_hand_adjustment=0):
        data = self.convert(obj)

        if on_hand_adjustment > 0:
            data['inventory_level'] = data.get('inventory_level') - on_hand_adjustment

        # bigcommerce doesn't like negative/overallocated inventory
        if data.get('inventory_level', 0) < 0:
            data['inventory_level'] = 0

        return data

    @staticmethod
    def _get_available(obj):
        return obj.get('_available', 0)
