# A list of country IS02's with no states mappedin in BC
iso2_country_state_exclusions = [
    'AF', #	Afghanistan
    'DZ', #	Algeria
    'AD', #	Andorra
    'AI', #	Anguilla
    'AG', #	Antigua and Barbuda
    'AM', #	Armenia
    'AZ', #	Azerbaijan
    'BH', #	Bahrain
    'BB', #	Barbados
    'BE', #	Belgium
    'BJ', #	Benin
    'BT', #	Bhutan
    'BA', #	Bosnia and Herzegovina
    'BV', #	Bouvet Island
    'IO', #	British Indian Ocean Territory
    'BG', #	Bulgaria
    'BI', #	Burundi
    'CM', #	Cameroon
    'CV', #	Cape Verde
    'CF', #	Central African Republic
    'CL', #	Chile
    'CX', #	Christmas Island
    'CO', #	Colombia
    'CG', #	Congo
    'CK', #	Cook Islands
    'HR', #	Croatia
    'CY', #	Cyprus
    'DK', #	Denmark
    'DM', #	Dominica
    'EC', #	Ecuador
    'SV', #	El Salvador
    'ER', #	Eritrea
    'ET', #	Ethiopia
    'FO', #	Faroe Islands
    'FI', #	Finland
    'GF', #	French Guiana
    'TF', #	French Southern Territories
    'GM', #	Gambia
    'GI', #	Gibraltar
    'GL', #	Greenland
    'GP', #	Guadeloupe
    'GT', #	Guatemala
    'GW', #	Guinea-Bissau
    'HT', #	Haiti
    'VA', #	Holy See (Vatican City State)
    'HK', #	Hong Kong
    'IS', #	Iceland
    'IQ', #	Iraq
    'IL', #	Israel
    'JM', #	Jamaica
    'JO', #	Jordan
    'KE', #	Kenya
    'KR', #	Korea, Republic of
    'KG', #	Kyrgyzstan
    'LB', #	Lebanon
    'LR', #	Liberia
    'LI', #	Liechtenstein
    'LU', #	Luxembourg
    'MK', #	Macedonia, the Former Yugoslav Republic of
    'MW', #	Malawi
    'MV', #	Maldives
    'MT', #	Malta
    'MQ', #	Martinique
    'MU', #	Mauritius
    'MD', #	Moldova, Republic of
    'MN', #	Mongolia
    'MA', #	Morocco
    'NR', #	Nauru
    'NL', #	Netherlands
    'NC', #	New Caledonia
    'NI', #	Nicaragua
    'NG', #	Nigeria
    'NF', #	Norfolk Island
    'NO', #	Norway
    'PK', #	Pakistan
    'PS', #	Palestinian Territory, Occupied
    'PG', #	Papua New Guinea
    'PE', #	Peru
    'PN', #	Pitcairn
    'PT', #	Portugal
    'QA', #	Qatar
    'RO', #	Romania
    'RW', #	Rwanda
    'KN', #	Saint Kitts and Nevis
    'PM', #	Saint Pierre and Miquelon
    'WS', #	Samoa
    'ST', #	Sao Tome and Principe
    'SN', #	Senegal
    'SC', #	Seychelles
    'SG', #	Singapore
    'SI', #	Slovenia
    'SO', #	Somalia
    'GS', #	South Georgia and the South Sandwich Islands
    'LK', #	Sri Lanka
    'SR', #	Suriname
    'SZ', #	Swaziland
    'TW', #	Taiwan
    'TZ', #	Tanzania, United Republic of
    'TL', #	Timor-Leste
    'TK', #	Tokelau
    'TT', #	Trinidad and Tobago
    'TR', #	Turkey
    'TC', #	Turks and Caicos Islands
    'UG', #	Uganda
    'UY', #	Uruguay
    'VU', #	Vanuatu
    'VN', #	Viet Nam
    'VI', #	Virgin Islands, U.S.
    'EH', #	Western Sahara
    'ZM', #	Zambia
    'IM', #	Isle of Man
    'GG', #	Guernsey
    'KP', #	Korea, Democratic People's Republic of
    'CI', #	Cote d'Ivoire
    'XK', #	Republic of Kosovo
    'AL', #	Albania
    'AS', #	American Samoa
    'AO', #	Angola
    'AQ', #	Antarctica
    'AW', #	Aruba
    'BS', #	Bahamas
    'BD', #	Bangladesh
    'BY', #	Belarus
    'BZ', #	Belize
    'BM', #	Bermuda
    'BO', #	Bolivia
    'BW', #	Botswana
    'BR', #	Brazil
    'BN', #	Brunei Darussalam
    'BF', #	Burkina Faso
    'KH', #	Cambodia
    'KY', #	Cayman Islands
    'TD', #	Chad
    'CN', #	China
    'CC', #	Cocos (Keeling) Islands
    'KM', #	Comoros
    'CD', #	Congo, the Democratic Republic of the
    'CR', #	Costa Rica
    'CU', #	Cuba
    'CZ', #	Czech Republic
    'DJ', #	Djibouti
    'DO', #	Dominican Republic
    'EG', #	Egypt
    'GQ', #	Equatorial Guinea
    'EE', #	Estonia
    'FK', #	Falkland Islands (Malvinas)
    'FJ', #	Fiji
    'FR', #	France
    'PF', #	French Polynesia
    'GA', #	Gabon
    'GE', #	Georgia
    'GH', #	Ghana
    'GR', #	Greece
    'GD', #	Grenada
    'GU', #	Guam
    'GN', #	Guinea
    'GY', #	Guyana
    'HM', #	Heard Island and Mcdonald Islands
    'HN', #	Honduras
    'HU', #	Hungary
    'IR', #	Iran, Islamic Republic of
    'IT', #	Italy
    'JP', #	Japan
    'KZ', #	Kazakhstan
    'KI', #	Kiribati
    'KW', #	Kuwait
    'LV', #	Latvia
    'LS', #	Lesotho
    'LY', #	Libyan Arab Jamahiriya
    'LT', #	Lithuania
    'MO', #	Macao
    'MG', #	Madagascar
    'ML', #	Mali
    'MH', #	Marshall Islands
    'MR', #	Mauritania
    'YT', #	Mayotte
    'FM', #	Micronesia, Federated States of
    'MC', #	Monaco
    'MS', #	Montserrat
    'MZ', #	Mozambique
    'NA', #	Namibia
    'NP', #	Nepal
    'AN', #	Netherlands Antilles
    'NZ', #	New Zealand
    'NE', #	Niger
    'NU', #	Niue
    'MP', #	Northern Mariana Islands
    'OM', #	Oman
    'PW', #	Palau
    'PA', #	Panama
    'PY', #	Paraguay
    'PH', #	Philippines
    'PL', #	Poland
    'PR', #	Puerto Rico
    'RE', #	Reunion
    'RU', #	Russian Federation
    'SH', #	Saint Helena
    'LC', #	Saint Lucia
    'VC', #	Saint Vincent and the Grenadines
    'SM', #	San Marino
    'SA', #	Saudi Arabia
    'RS', #	Serbia
    'SL', #	Sierra Leone
    'SK', #	Slovakia
    'SB', #	Solomon Islands
    'SD', #	Sudan
    'SJ', #	Svalbard and Jan Mayen
    'SE', #	Sweden
    'SY', #	Syrian Arab Republic
    'TJ', #	Tajikistan
    'TH', #	Thailand
    'TG', #	Togo
    'TO', #	Tonga
    'TN', #	Tunisia
    'TM', #	Turkmenistan
    'TV', #	Tuvalu
    'UA', #	Ukraine
    'GB', #	United Kingdom
    'UM', #	United States Minor Outlying Islands
    'UZ', #	Uzbekistan
    'VE', #	Venezuela
    'VG', #	Virgin Islands, British
    'WF', #	Wallis and Futuna
    'YE', #	Yemen
    'ZW', #	Zimbabwe
    'JE', #	Jersey
    'LA', #	Lao People's Democratic Republic
    'ME', #	Montenegro
    'BQ', #	Bonaire, Sint Eustatius and Saba
]
